from bcc import BPF
from utils import *
from pwn import ELF
import ctypes as ct
import sys, subprocess, os, docker, re, argparse, threading, psutil


ba_cache = {} # base address cache
call_stack = {}
kill_list = set()
traced_binaries = set()
containers = {}

def update_ba_cache(pid):
	try:
		if pid not in ba_cache:
			call_stack[pid] = []
			ba_cache[pid] = {}
			# find base addresses of the libs of the process
			with open('/proc/%d/maps' % pid, 'r') as maps:
				matches = re.findall(r'([0-9a-fA-F]+).+r-xp.+?(\/.+)', maps.read())
				for base, lib in matches:
					ba_cache[pid][lib] = int(base, 16)
	except FileNotFoundError:
		log.exception('pid/file error @ba_cache_update.')


def get_tfunc(data, lmb):
	update_ba_cache(data.pid)
	for bin in traced_binaries:
		try:
			addr = data.eip - (ba_cache[data.pid][bin.path] if bin.is_pie else 0)
			for func in bin.traced_functions:
				if lmb(func, addr):
					return func
		except KeyError:
			continue

def probe_entry_event(cpu, data, size):
	data = bpf["entry_events"].event(data)
	func = get_tfunc(data, lambda func, addr: func.entry_addr == addr)
	log.debug(traced_binaries)

	if not func:
		log.debug('%016x\n' % data.eip)
		return

	if call_stack[data.pid] and func.name not in call_stack[data.pid][-1].calls:
		kill_list.add(data.pid)
		log.critical('%d - wrong order of calls (%s)' % (data.pid, func.name))

	call_stack[data.pid].append(func)
	log.debug('%d %08x %s_entry' % (data.pid, data.eip, func.name))


def probe_exit_event(cpu, data, size):
	data = bpf["exit_events"].event(data)
	func = get_tfunc(data, lambda func, addr: addr in func.exit_addr)
	log.debug(call_stack)
	log.debug(traced_binaries)
	
	if call_stack[data.pid]:
		last = call_stack[data.pid][-1].name
	else:
		last = '_unknown'
	
	if last != func.name:
		pass
		
	elif call_stack[data.pid]:
		call_stack[data.pid].pop()

	log.debug('%d %08x %s_exit' % (data.pid, data.eip, func.name))


def execve_event(cpu, data, size):
	data = bpf["exec_events"].event(data)
	
	try:
		parents = psutil.Process(data.pid).parents()
		for proc in parents:
			if proc.pid in kill_list:
				log.critical('Attempting to kill process with pid %d.' % (data.pid))
				try:
					
					os.kill(data.pid, 9)
				except ProcessLookupError:
					log.error("process %d wasn't found." % data.pid)
				break
	except:
		pass
	

def gdb_disasm(path, sym):
	cmd = "gdb %s -batch -ex 'set disassembly-flavor intel' -ex 'disassemble %s'" % (path, sym)
	output = subprocess.check_output(cmd, shell=True).decode()
	addrs = re.findall(r'\n   (0x[0-9a-fA-F]+)(?:.+?(ret)|)', output)
	addrs = map(lambda a: int(a[0], 16), [addrs.pop(0), addrs.pop(-1), *[a for a in addrs if a[1]]])
	return re.findall(r'call.+<(.+?)([\+@>])', output), list(addrs)


def analyze_binary(path, externals=None):
	global traced_binaries
	bin = Executable(path)
	elf = ELF(path)
	bin.is_pie = elf.pie
	res = set()

	funcs = set(elf.functions.keys())
	if externals is not None:
		funcs = funcs.intersection(externals)

	funcs = list(funcs)
	externals = set()
	for sym in funcs:
		# skip problematic functions
		if sym[0] in ['*', '_'] or sym in ['setvbuf', 'puts', 'execve']:
			continue

		try:
			calls, addrs = gdb_disasm(path, sym)
		except IndexError:
			continue
			
		tfunc = TracedFunction(name=sym, entry_addr=addrs[0], exit_addr=addrs[1::])

		for call, type in calls:
			tfunc.calls.append(call)
			if type == '@': # plt/got (external)
				externals.add(call)
			elif call not in funcs:
				funcs.append(call)

		bin.traced_functions.append(tfunc)

	
	res.add(bin)
	for lib in [_ for _ in elf.libs if _ != path]:
		if list(filter(lambda _: _.path == lib, traced_binaries)):
			continue
		res.update(analyze_binary(lib, externals))
			
	return res

def list_executables(path):
	cmd = """find %s -type f -executable -exec file '{}' \; | grep 'LSB executable'""" % path
	try:
		output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT).decode().strip()
		return [_.split(": ")[0] for _ in output.split("\n")]
	except subprocess.CalledProcessError:
		return []

def attach_probes(found):
	for elf in found:
		for func in elf.traced_functions:
			log.debug("Attaching probes to %s." % func.name)
			# attach uprobe to every entry
			try:
				bpf.attach_uprobe(name=elf.path, sym=func.name, fn_name="entry_probe")
			except: # uprobe same thing twice
				continue 
			
			# attach uprobe to every exit
			for exit in func.exit_addr:
				bpf.attach_uprobe(	name=elf.path, \
									sym=func.name, \
									fn_name="exit_probe", \
									sym_off=exit-func.entry_addr)

def detach_probes(found):
	for elf in found:
		for func in elf.traced_functions:
			log.debug("Detaching probes from %s." % func.name)
			bpf.detach_uprobe(name=elf.path, sym=func.name)
			for exit in func.exit_addr:
				bpf.detach_uprobe(	name=elf.path, \
									sym=func.name, \
									sym_off=exit-func.entry_addr)

def manage_probes():
	global traced_binaries
	cli = docker.from_env()
	
	for event in cli.events(decode=True):
		status = event.get("status")
		if status == "start":
			id = event["id"]
			with open(f"/var/lib/docker/image/overlay2/layerdb/mounts/{id}/mount-id", "r") as f:
				path = "/var/lib/docker/overlay2/{}/merged".format(f.read())
				link = "/tmp/ss/" + id[:10]
				os.symlink(path, link)
				log.info("New container found, Starting probe attachment...")
				log.debug(path + ", " + link)
				
				for elf in list_executables(link + "/"):
					if "/usr/" in elf:
						continue
					res = analyze_binary(elf) - traced_binaries
					containers[id] = res
					traced_binaries.update(res)
					attach_probes(res)
					
		elif status == "die":
			link = "/tmp/ss/" + event["id"][:10]
			log.info("Attempting to delete symlink " + link)
			try:
				os.remove(link)
				#detach_probes(containers[event["id"]])
			except (FileNotFoundError, KeyError):
				pass

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="BPF ret2libc protector")
	parser.add_argument("-d", "--debug", dest="debug_enabled", action="store_true", help="Output debug logs.")
	args = parser.parse_args()
	log = init_logger("protector", args.debug_enabled)

	log.info("Loading bpf code...")
	try:
		with open("bpf_probes.c", 'r') as f:
			bpf = BPF(text=f.read())
		log.info("BPF loaded successfully.")
	except FileNotFoundError:
		log.critical("bpf_probes.c wasn't found, exiting...")
		exit(-1)

	# probe execve and analyze binaries
	bpf.attach_kprobe(event=bpf.get_syscall_fnname("execve"), fn_name="execve_probe")
	bpf["entry_events"].open_perf_buffer(probe_entry_event)
	bpf["exit_events"].open_perf_buffer(probe_exit_event)
	bpf["exec_events"].open_perf_buffer(execve_event)
	
	log.info("Starting receiving thread...")
	p = threading.Thread(target=manage_probes)
	p.daemon = True
	p.start()
	
	try:
		while 1:
			bpf.kprobe_poll()
	except KeyboardInterrupt:
		pass
	
	log.warn("Stopping...")
	
	
