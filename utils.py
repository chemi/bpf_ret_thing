import logging
from colorlog import ColoredFormatter

class TracedFunction:
	def __init__(self, name, entry_addr, exit_addr, calls=[]):
		self.name = name
		self.entry_addr = entry_addr
		self.exit_addr = exit_addr
		self.calls = calls

class Executable:
	def __init__(self, path):
		self.path = path
		self.name = path.split('/')[-1]
		self.traced_functions = []
		self.is_pie = False
		#self.elf = ELF(path)

def init_logger(name, debug_enabled=False):
	formatter = ColoredFormatter(
		'\033[1m'
		'%(log_color)s'
		'%(asctime)s - '
		'%(name)s - '
		'%(funcName)s - '
		'%(levelname)s - '
		'%(message)s',
		reset=True,
		log_colors={
			'DEBUG':    'cyan',
			'INFO':     'green',
			'WARNING':  'yellow',
			'ERROR':    'red',
			'CRITICAL': 'red',
		}
	)
	
	log = logging.getLogger(name)
	handler = logging.StreamHandler()
	handler.setFormatter(formatter)
	log.addHandler(handler)

	if debug_enabled:
		log.setLevel(logging.DEBUG)
	else:
		log.setLevel(logging.INFO)

	return log