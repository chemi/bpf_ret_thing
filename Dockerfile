FROM ubuntu:bionic

RUN set -ex; \
	echo "deb [trusted=yes] http://repo.iovisor.org/apt/bionic bionic-nightly main" > /etc/apt/sources.list.d/iovisor.list; \
	dpkg --add-architecture i386; \
	apt-get update -y; \
	DEBIAN_FRONTEND=noninteractive \
	apt-get install -y cmake python3 python3-pip python3-dev python3-bcc git libssl-dev libffi-dev build-essential; \
	apt-get install -y libc6:i386 libc6-dbg:i386 libncurses5:i386 libstdc++6:i386;
	
#RUN set -ex; \
#	git clone https://github.com/iovisor/bcc.git; \
#	mkdir bcc/build; cd bcc/build; \
#	cmake .. -DCMAKE_INSTALL_PREFIX=/usr; \
#	make; \
#	make install; \
#	cmake -DPYTHON_CMD=python3 .. # build python3 binding; \
#	pushd src/python/; \
#	make; \
#	make install; \
#	popd;

#RUN python3 -m pip install colorlog
#RUN python3 -m pip install --upgrade git+https://github.com/Gallopsled/pwntools.git@dev

COPY ./utils.py ./bpf_probes.c ./probe.py ./poc /Rezilion/
RUN cp /Rezilion/poc /Rezilion/poc2

ENV TERM=linux
ENV TERMINFO=/etc/terminfo



#CMD cd /Rezilion/ && python3 ./probe.py