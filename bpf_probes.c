#include <uapi/linux/ptrace.h>
#include <linux/sched.h>
#include <linux/fs.h>

struct data_t {
		u32 pid;
		u64 eip;
		char comm[TASK_COMM_LEN];
};

BPF_PERF_OUTPUT(entry_events);
BPF_PERF_OUTPUT(exit_events);
BPF_PERF_OUTPUT(exec_events);

void entry_probe(struct pt_regs *ctx)
{
	struct data_t data = {};
    struct task_struct *task;

	bpf_get_current_comm(&data.comm, sizeof(data.comm));
	data.pid = bpf_get_current_pid_tgid();
	data.eip = ctx->ip;

	entry_events.perf_submit(ctx, &data, sizeof(data));
}

void exit_probe(struct pt_regs *ctx)
{
	struct data_t data = {};
    struct task_struct *task;

	bpf_get_current_comm(&data.comm, sizeof(data.comm));
	data.pid = bpf_get_current_pid_tgid();
	data.eip = ctx->ip;

	exit_events.perf_submit(ctx, &data, sizeof(data));
}

void execve_probe(struct pt_regs *ctx)
{
	struct data_t data = {};
    struct task_struct *task;

	bpf_get_current_comm(&data.comm, sizeof(data.comm));
	data.pid = bpf_get_current_pid_tgid();
	data.eip = ctx->ip;

	exec_events.perf_submit(ctx, &data, sizeof(data));
}